const jwt = require("jsonwebtoken");
const Unauthenticated = require("../errors");


const auth = async (req, res, next)=>{
    const authHeader = req.headers.authorization;

    // if(req.url === "/" && req.method === "GET")
    // {
    //     next();
    // }

    if(!authHeader || !authHeader.startsWith("Bearer "))
    {
        throw new Unauthenticated("Authorization invalid");
    }

    const token = authHeader.split(" ")[1];

    try
    {
        const decoded = jwt.verify(token, process.env.SECRET_KEY);

        //console.log(decoded);

        req.user = {userId: decoded.userId, name: decoded.name};
        next();
    }
    catch(error)
    {
        throw new Unauthenticated("Authorization invalid");
    }
}

module.exports = auth;