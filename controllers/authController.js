const User = require("../models/User");
const {StatusCodes} = require("http-status-codes");
const {BadRequest, Unauthenticated} = require("../errors");

const register = async (req, res)=>{

    const user = await User.create({...req.body});

    const token = user.generateToken();

    res.status(StatusCodes.CREATED).json({user: {userId: user._id, name: user.name}, token});
}

const login = async (req, res)=>{
    const {email, password} = req.body;

    if(!email || !password)
    {
        throw new BadRequest("Please enter email and password");
    }

    const user = await User.findOne({email: email});

    if(!user)
    {
        throw new Unauthenticated("User doesn't exist");
    }

    const correctPassword = await user.validatePassword(password);

    if(!correctPassword)
    {
        throw new Unauthenticated("email or password is incorrect");
    }

    const token = user.generateToken();

    res.status(StatusCodes.OK).json({user: {name: user.name, userId: user._id}, token});
}

module.exports = {register, login};