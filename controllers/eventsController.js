const Event = require("../models/Event");
const {StatusCodes} = require("http-status-codes");
const {BadRequest, NotFound, Unauthenticated} = require("../errors");

const getAllEvents = async (req, res)=>{

    //console.log("here");
    const events = await Event.find({private: false}).sort("-createdAt");

    res.status(StatusCodes.OK).json({events});
}

const getEvent = async (req, res)=>{
    //const {userId} = req.user;
    const {id} = req.params;

    const event = await Event.findOne({_id: id});

    if(!event)
    {
        throw new NotFound("Event not found");
    }
    res.status(StatusCodes.OK).json({event});
}

const createEvent = async (req, res)=>{
    const {userId} = req.user;

    req.body.createdBy = userId;

    const event = await Event.create(req.body);

    res.status(StatusCodes.CREATED).json({event});
}

const updateEvent = async (req, res)=>{
    const {userId} = req.user;
    const {id} = req.params;

    const {name, description, imageUrl, price, location, date} = req.body;

    if(name === "" || description === "" || imageUrl === "" || price === "" || location === "" || date === "")
    {
        throw new BadRequest("Please make sure all mandatory fields are entered");
    }

    try
    {
    if(Number(price) < 0)
    {
        throw new BadRequest("Price must be 0 or more")
    }
    }
    catch(error)
    {
        throw new BadRequest("Price must be a number");
    }

    //console.log(userId);
    const event = await Event.findOneAndUpdate({createdBy: userId, _id: id}, req.body, {new: true, runValidators: true});

    if(!event)
    {
        throw new Unauthenticated("Not authorized to edit this event");
    }

    res.status(StatusCodes.OK).json({event});
}

const deleteEvent = async (req, res)=>{
    const {userId} = req.user;
    const {id} = req.params;

    const event = await Event.findOneAndRemove({createdBy: userId, _id: id});

    if(!event)
    {
        throw new Unauthenticated("Not authorised to delete this event");
    }

    res.status(StatusCodes.OK).send();
}

module.exports = {getAllEvents, getEvent, createEvent, updateEvent, deleteEvent};