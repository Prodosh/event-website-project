require("dotenv").config();
require("express-async-errors");


const express = require("express");

const helmet = require("helmet");
const cors = require("cors");
const xss = require("xss-clean");
const rateLimiter = require("express-rate-limit");

const conDb = require("./db/conDb");

const app = express();

const PORT = process.env.PORT || 3000;

const errorHandler = require("./middleware/errorHandler");
const tokenAuth = require("./middleware/authenticate");

app.set("trust proxy", 1);

app.use(rateLimiter({
    windowMs: 15 * 60 * 1000,
    max: 100
}));

app.use(express.json());
app.use(helmet());
app.use(cors());
app.use(xss());





app.use(express.static("./public"));

const eventsRouter = require("./routes/eventsRoute");
const authRouter = require("./routes/authRoute");

app.use("/api/auth", authRouter);
app.use("/api/events", tokenAuth, eventsRouter);

app.use(errorHandler);

const start = async ()=>{

    try{
        await conDb(process.env.MONGO_URI);
        app.listen(PORT, ()=>{
            
            console.log(`Server listening on port ${PORT}`);
        })
    }
    catch (error){
        console.log(error);
    }

}

start();
