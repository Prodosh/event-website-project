const mongoose = require("mongoose");

const eventSchema = new mongoose.Schema({
    name:{
        type: String,
        required: [true, "Please provide event name"],
        minlength: 3
    },
    imageUrl:{
        type: String,
        required: [true, "Please provide image url"]
    },
    description:{
        type: String,
        required: [true, "Please provide event description"],
        minlength: 3
    },
    price:{
        type: Number,
        required: [true, "Please provide event price"]
    },
    adult:{
        type: Boolean,
        default: false
    },
    location:{
        type: String,
        required: [true, "Please provide event location"]
    },
    date:{
        type: String,
        required: [true, "Please specify date and time of event"]
    },
    private:{
        type: Boolean,
        default: false
    },
    createdBy:{
        type: mongoose.Types.ObjectId,
        ref: "User",
        required: [true, "Please provide id of host user"]
    }

}, {timestamps: true})

module.exports = mongoose.model("Event", eventSchema);