const mongoose = require("mongoose");
const jwt = require("jsonwebtoken");
const bcrypt = require("bcryptjs");

const userSchema = new mongoose.Schema({
    name:{
        type: String,
        required: [true, "Please provide name"],
        minlength: 3,
        maxlength: 60
    },
    age:{
        type: Number,
        required: [true, "Please provide age"]
    },
    email:{
        type: String,
        required: [true, "Please provide email"],
        match: [/^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/, "Please provide valid email"],
        unique: true
    },
    password:{
        type: String,
        required: [true, "Please provide password"],
        minlength: 6
    }
})

userSchema.methods.generateToken = function()
{
    return jwt.sign({userId: this._id, name: this.name}, process.env.SECRET_KEY, {expiresIn: "10d"});
}

userSchema.methods.validatePassword = async function(password)
{
    const isValid =  await bcrypt.compare(password, this.password);

    return isValid;
}

userSchema.pre("save", async function(next){
    const salt = await bcrypt.genSalt(10);

    this.password = await bcrypt.hash(this.password, salt);

    next();
})


module.exports = mongoose.model("User", userSchema);