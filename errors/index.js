const CustomError = require('./customError')
const Unauthenticated = require('./unauthen')
const NotFound = require('./notFound')
const BadRequest = require('./badRequest')

module.exports = {
    CustomError,
    Unauthenticated,
    NotFound,
    BadRequest,
}
